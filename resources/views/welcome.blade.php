<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Styles -->

</head>

<body ng-app="MyApp" ng-controller="MasterController">
    <div class="flex-center position-ref full-height">

        <div class="container-fluid pt-5">
            <div class="form-row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">

                            <div class="alert alert-success" ng-if="message">
                                <p>@{{message}}</p>
                            </div>
                            <form>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input type="email" class="form-control" ng-model="data.email"
                                        id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                                    <small id="emailHelp" class="form-text text-danger"
                                        ng-bind="error.email[0]"></small>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Contact</label>
                                    <input type="text" class="form-control" ng-model="data.contact"
                                        id="exampleInputPassword1" placeholder="Enter Contact">
                                    <small id="emailHelp" class="form-text text-danger"
                                        ng-bind="error.contact[0]"></small>

                                </div>
                                <button type="submit" class="btn btn-primary" ng-click="AddStore(data)">
                                    <span ng-if="!data.id">Submit</span>
                                    <span ng-if="data.id">Update</span>
                                </button>
                                <button type="submit" class="btn btn-danger">Clear</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="d-flex justify-content-end">
                            <div class="p-2">
                                <button type="button" class="btn btn-info btn-sm" ng-click="refresh()">
                                    Refresh List
                                </button>
                            </div>
                        </div>
                        <div class="card-body">

                            <div class="alert alert-success" ng-if="de_message">
                                <p>@{{de_message}}</p>
                            </div>

                            <table class="table text-center">
                                <thead>
                                    <th>#</th>
                                    <th>Email</th>
                                    <th>Contact</th>
                                    <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr dir-paginate="value in list |filter:q | itemsPerPage:limit" total-items="count"
                                        current-page="page" pagination-id="category_list_paginate">

                                        <td ng-bind="((page-1)*limit)+($index+1)"></td>

                                        <td>@{{value.email}}</td>
                                        <td>@{{value.contact}}</td>
                                        <td>
                                            <button type="button" class="btn btn-info btn-sm"
                                                ng-click="editStore(value)">
                                                Edit
                                            </button>
                                            <button type="button" class="btn btn-danger btn-sm"
                                                ng-click="deleteStore(value)">
                                                Delete
                                            </button>
                                        </td>
                                    </tr>
                                    <tr ng-if="list.length == 0">
                                        <td colspan="4">
                                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                <strong>No Record Found!</strong>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="d-flex justify-content-center">
                                <div class="p-2">
                                    <dir-pagination-controls pagination-id="category_list_paginate"
                                        on-page-change="get_test_list(newPageNumber)"
                                        template-url="/lib/angular-utils-pagination/dirPagination.tpl.html">
                                    </dir-pagination-controls>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="/js/jquery.min.js"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/angular.min.js"></script>
    <script src="/js/app.js"></script>
    <script src="/lib/angular-utils-pagination/dirPagination.js"></script>

</body>

</html>