<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/get_test_list', 'TestController@get_test_list');

Route::post('/get_store_list', 'TestController@get_store_list');

Route::post('/get_edit_list', 'TestController@get_edit_list');

Route::post('/get_delete_list', 'TestController@get_delete_list');

