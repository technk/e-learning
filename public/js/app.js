
var app = angular.module("MyApp", [
    "angularUtils.directives.dirPagination",
]);

app.factory("master_service", function ($http) {
    return {
        get_list: function (url) {
            return $http.get(url).then(function (data) {
                return data.data;
            });
        },
        post_list: function (url, obj) {
            return $http.post(url, obj).then(function (data) {
                return data.data;
            });
        },
        delete: function (url, id) {
            return $http.delete(url + "/" + id).then(function (data) {
                return data.data;
            });
        }
    };
});

app.controller("MasterController", function (
    $http,
    master_service,
    $scope,
    $timeout
) {
    console.log("Log");
    $scope.page = 1;
    $scope.limit = 3;

    $scope.get_test_list = function (page) {
        var q = {
            items_per_page: $scope.limit,
            current_page_no: $scope.page,
        };
        master_service.post_list("/get_test_list", q).then(function (result) {
            $scope.list = result.data;
            $scope.count = result.count;
            // console.log(result.data);
        });
    };
    $scope.get_test_list($scope.page);

    $scope.data = {
        id: null,
    };

    $scope.AddStore = function (data) {
        master_service
            .post_list("/get_store_list", data)
            .then(function (result) {
                if (result.status == "error") {
                    $scope.error = result.errors;
                } else {
                    $scope.error = "";
                    $scope.message = result.message;
                    $scope.get_test_list($scope.page);
                    $scope.data = {
                        id: null,
                    };
                }
                $timeout(() => {
                    $scope.message = null;
                }, 3000);
            });
    };
    $scope.editStore = function (data) {
        master_service
            .post_list("/get_edit_list", data)
            .then(function (result) {
                $scope.data = result.data;
            });
    };
    $scope.deleteStore = function (data) {
        master_service
            .post_list("/get_delete_list", data)
            .then(function (result) {
                $scope.de_message = result.message;
                $scope.get_test_list($scope.page);
                      $timeout(() => {
                          $scope.de_message = null;
                      }, 3000);
            });
    };

    $scope.reset = function () {
        $scope.data = {
            id: null,
        };
    };
    $scope.refresh = function () {
       $scope.get_test_list($scope.page);
    };
});