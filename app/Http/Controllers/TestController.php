<?php

namespace App\Http\Controllers;

use App\Model\Test;
use App\Jobs\TestJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_test_list()
    {
        //
        // $test = Test::paginate(5);
        $items_per_pages = Input::get('items_per_page');
        $current_page_nos = Input::get('current_page_no');

        if (isset($items_per_pages, $current_page_nos)) {
        $current_page_starting_no = ($items_per_pages * ($current_page_nos - 1));
        $count = Test::count();
        $test = Test::skip($current_page_starting_no)
            ->take($items_per_pages)
            ->get();

            return  array('data'=>$test,'count'=>$count);
    } 

      
    }

    public function get_store_list(Request $request)
    {
           $rules = [
            'email' => 'required|email',
            'contact' => 'required|numeric',
            
        ];

        $checkvalid = \Validator::make($request->all(), $rules);
        if ($checkvalid->fails()) {
            $obj = array('status' => 'error', 'message' => 'Validation Failed', 'errors' => $checkvalid->errors());
        } else {
            $request_toarray = $request->toArray();
            TestJob::dispatch($request_toarray);
                // ->delay(now()->addSeconds(10));
                if($request_toarray['id'] != null){
                  $message = 'update';      
                }else{
                    $message = 'added';      
                }
           $obj = array('status' => 'success', 'message' => 'Test '.$message.' successfully'); 
        }
        return $obj;
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get_edit_list(Request $request)
    {
        $test = Test::find($request->input('id'));
        return  array('data'=>$test);

    }
    public function get_delete_list(Request $request)
    {
        $test = Test::find($request->input('id'))->delete();
        return  array('message'=>'Test deleted successfully');

    }


    
}