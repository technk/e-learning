<?php

namespace App\Jobs;

use App\Model\Test;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class TestJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $test;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($test)
    {
        //
        $this->test = $test;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
            if (!empty($this->test['id'])) {
                Test::find($this->test['id'])->update($this->test);
            } else {
                Test::create($this->test);
            }
    }
}
